import com.jayway.restassured.RestAssured
import com.jayway.restassured.response.Response
import org.apache.commons.lang3.StringUtils

@Grapes([
        @Grab('com.jayway.restassured:rest-assured:2.5.0'),
        @GrabExclude(group = 'org.codehaus.groovy', module = '*')
])

final CliBuilder cli = new CliBuilder(
        usage: 'groovy AnonymizeProject.groovy -u username -s siteUrl -j project',
        header: '\nParameters:\n')

cli.with {
    u(longOpt: 'user', 'Username', args: 1, required: true)
    s(longOpt: 'url', 'XNAT URL', args: 1, required: true)
    j(longOpt: 'project', 'Project to anonymize', args: 1, required: true)
    e(longOpt: 'exclusions', 'A list of sessions that should be *excluded* from modification. The list should either be a comma-separated string passed directly into this argument, or the name of a file where the entries are separated by *new lines*.', args: 1, required: false)
    k(longOpt: 'insecure-ssl', 'Add this flag to indicate that SSL issues should be ignored [e.g. for dev servers]')
}

final String LABEL = 'label'
final String XSI_TYPE = 'xsiType'

final OptionAccessor params = cli.parse(args)
final String username = params.u
final String site = params.s
final String project = params.j
final String password = System.console().readPassword("Password for XNAT account '${username}': ") as String
if (params.k) {
    println('Insecure SSL flag has been specified. SSL issues will be ignored.')
    RestAssured.useRelaxedHTTPSValidation()
}
// noinspection GroovyAssignabilityCheck
final List<String> exclusions = readExclusions(params.e ?: null)

String jsessionId = generateJsessionId(site, username, password)

final List<Map<String, String>> experimentMaps = RestAssured.given().
        sessionId(jsessionId).
        queryParams(['format': 'json', 'columns': "${LABEL},${XSI_TYPE}"]).
        get(url(site, "/data/projects/${project}/experiments")).
        then().assertThat().statusCode(200).and().
        extract().path('ResultSet.Result')

final Map<String, String> sessions = [:]
final Map<String, String> nonsessions = [:]

experimentMaps.each { experiment ->
    final String xsiType = experiment[XSI_TYPE]
    final String label = experiment[LABEL]
    if (xsiType.matches('xnat:.+SessionData')) {
        sessions.put(label, xsiType)
    } else {
        nonsessions.put(label, xsiType)
    }
}

if (nonsessions.isEmpty()) {
    println("Project ${project} doesn't seem to contain any non-imaging subject assessors.")
} else {
    println("Project ${project} contains some experiments that seem to be non-imaging subject assessors. These experiments will *NOT* be handled. The experiments with their corresponding xsiTypes are: ${nonsessions}")
}

println("${sessions.size()} imaging sessions have been read in project '${project}'. The sessions with their corresponding xsiTypes are: ${sessions}")

final List<String> sessionLabels = sessions.keySet() as List
if (!exclusions.isEmpty()) {
    println("The following sessions have been requested for exclusion from modification: ${exclusions}.")
    final int initialSize = sessionLabels.size()
    sessionLabels.removeAll(exclusions)
    final int newSize = sessionLabels.size()
    println("${initialSize - newSize} sessions have been filtered out. The remaining sessions are: ${sessionLabels}")
}

final int totalNumSessions = sessionLabels.size()

final String suffix = ['_ANON', '_TMPANON', '_ANONTEMP', '_TMP_RELABEL', '_RELABEL_TEMP'].find { possibleSuffix ->
    !sessionLabels.any { session ->
        session.contains(possibleSuffix)
    }
}
if (suffix) {
    println("Suffix '${suffix}' will be appended to all session labels temporarily.")
} else {
    throw new RuntimeException('Could not find an appropriate suffix to use for relabeling.')
}

System.console().readPassword("Unless it is acceptable to anonymize the data twice, *disable* the project's anonymization script at this time. When this is done, press the ENTER key to confirm")
println('Regenerating JSESSIONID for authentication...')
jsessionId = generateJsessionId(site, username, password)

final List<String> failedSessions = sessionLabels.indexed(1).findResults { index, session ->
    final int statusCode = move(jsessionId, site, project, session, "${session}${suffix}")
    if (statusCode == 200) {
        println("Successfully renamed ${session} with temporary label. [${index}/${sessionLabels.size()}]")
        null
    } else {
        println("Renamimg for ${session} with temporary label did not report a success [HTTP 200]. REST call instead returned status code ${statusCode}.")
        session
    }
}

System.console().readPassword("Relabeling has been attempted for all sessions in the project. *Enable* the project's anonymization script at this time and set the script to the desired value. When this is done, press the ENTER key to confirm")
println('Regenerating JSESSIONID for authentication...')
jsessionId = generateJsessionId(site, username, password)

sessionLabels.removeAll(failedSessions)
final List<String> restoreFailed = sessionLabels.indexed(1).findResults { index, session ->
    final int statusCode = move(jsessionId, site, project, "${session}${suffix}", session)
    if (statusCode == 200) {
        println("Successfully restored session label: ${session}. [${index}/${sessionLabels.size()}]")
        null
    } else {
        println("Renamimg for ${session} with temporary label did not report a success [HTTP 200]. REST call instead returned status code ${statusCode}.")
        session
    }
}
sessionLabels.removeAll(restoreFailed)

println("Attempt to anonymize sessions in project '${project}' has concluded.")
println("\t[${sessionLabels.size()}/${totalNumSessions}] sessions have been anonymized correctly: ${sessionLabels}")
println("\t[${failedSessions.size()}/${totalNumSessions}] sessions did not report correct label changing in the *first pass*. These sessions may appear with their original session labels, or with the suffix '${suffix}' appended. Original labels: ${failedSessions}")
println("\t[${restoreFailed.size()}/${totalNumSessions}] sessions did not report correct label changing in the *second pass*. These sessions may appear with their original session labels, or with the suffix '${suffix}' appended. Original labels: ${restoreFailed}")

// methods

List<String> readExclusions(String exclusionParam) {
    if (exclusionParam) {
        final File exclusionFile = new File(exclusionParam)
        if (exclusionFile.exists()) {
            println("Exclusion file found: ${exclusionParam}...")
            exclusionFile.readLines()
        } else {
            println("Exclusion file not found under parameter. Interpretting exclusion input as comma-separated list of sessions to exclude: ${exclusionParam}...")
            exclusionParam.split(',')
        }
    } else {
        []
    }
}

int move(String jsessionId, String siteUrl, String project, String original, String newLabel) {
    RestAssured.given().sessionId(jsessionId).queryParam('label', newLabel).put(url(siteUrl, "/data/projects/${project}/experiments/${original}")).then().extract().response().statusCode
}

String url(String... fragments) {
    StringUtils.stripAll(fragments, '/').join('/')
}

String generateJsessionId(String site, String username, String password) {
    final Response authResponse = RestAssured.given().authentication().basic(username, password).get(url(site, '/data/auth'))
    if (authResponse.statusCode != 200) {
        throw new RuntimeException('Provided username and password do not seem to be correct [authentication failed].')
    }
    println('JSESSIONID has been read correctly from the server [authentication success].')
    authResponse.getSessionId()
}
