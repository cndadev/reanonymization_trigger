This is a simple script to force anonymization for all\* sessions in an XNAT project.


1.  First, the script should prompt you for the password to use for authentication.
2.  Next, the script should prompt you to turn *OFF* the project's anonymization script. This is to ensure that anonymization is not performed twice.
3.  Then, the script should change the labels of the sessions in the specified project by adding some suffix to the end of the labels.
4.  Finally the script should prompt you to turn *ON* the project's anonymization script. Then, it will restore the original session labels, which triggers XNAT to perform project anonymization.


## Prerequisites
This script requires a groovy (2.4+) installation with Java 7+.

## Parameters/Usage
Run the project by simply invoking the main script
```
#!bash

$ groovy AnonymizeProject.groovy <parameters>
```

The following parameters are all mandatory:


*  `-u USERNAME` : the XNAT username for authentication
*  `-s URL` : the URL for the XNAT server
*  `-j PROJECT_ID` : the project ID for the project to anonymize


The following parameters are optional:


*  `-k` : add this flag to allow the script to ignore SSL errors
*  `-e` : this is a list of sessions that should be *excluded* from modification. The list should either be a comma-separated string passed directly into this argument, or the name of a file where the entries are separated by *new lines*


Here are two examples on the exclusion flag:
```
#!bash

$ groovy AnonymizeProject.groovy -u admin -s https://myxnat.org -j PROJECT_A_01 -e "101ABC_MR1,101ABC_MR2"
$ groovy AnonymizeProject.groovy -u admin -s https://myxnat.org -j PROJECT_A_01 -e sample_exclusions.txt
```